# How to make a schedule

Schedules are JSON files that follow this pattern.

```
[
	{
		"regex": "Bird", // Matches any string with "Bird" in it
		"comment": "Early bird",
		"name": "PE"
	}
	{
		"regex": "^\\D*1\\D*$", // Matches any string with a 1 and no other numbers
		"comment": "1st Period",
		"name": "Your first-period class"
	},
	...
]
```

The `regex` property must match the name of the period.

Schedules can be added using the link in the bottom left. You can also find a graphical schedule builder there, but it's not very stable or well-made.

(Psst! Pro tip - you can put *any HTML you want* in your period's name! Try making your period name a link to your class's Zoom call. Make sure you use the `target` property so it doesn't open in the same tab.)
