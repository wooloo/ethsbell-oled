getel('upload').addEventListener('click', async event => {
	if (event.target === getel('upload')) {
		try {
			const file = await getel('schedule-file').files[0].text();
			const data = JSON.parse(file);
			console.log(data[0]);
			localStorage.setItem('schedule', file);
			getel('upload').innerHTML = 'success';
		} catch (error) {
			getel('upload').innerHTML = 'failed';
			console.error(error);
		}
	}
});

getel('download').addEventListener('click', async event => {
	if (event.target === getel('download')) {
		downloadString(localStorage.getItem('schedule'), 'application/json', 'schedule.json');
	}
});

function getel(id) {
	return document.querySelector(`#${id}`);
}

// https://gist.github.com/danallison/3ec9d5314788b337b682

function downloadString(text, fileType, fileName) {
	const blob = new Blob([text], {type: fileType});

	const a = document.createElement('a');
	a.download = fileName;
	a.href = URL.createObjectURL(blob);
	a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
	a.style.display = 'none';
	document.body.append(a);
	a.click();
	a.remove();
	setTimeout(() => {
		URL.revokeObjectURL(a.href);
	}, 1500);
}
