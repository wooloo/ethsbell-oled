let last;
let sched;
let notif = false;
let lastNotif = false;
let notifCurrentPeriod = false;

setInterval(async () => {
	await put(await get());
}, 3600000);
setInterval(async () => {
	await put(await get(last));
}, 5000);
(async () => {
	// Service Worker
	try {
		console.log('Installing service worker...');
		navigator.serviceWorker.register('/offline.js', {scope: '/'}).catch(error => {
			console.error(`Whoops, service worker installation failed with ${error}`);
		});
	} catch {
		console.error('Whoops, it doesn\'t look like this browser supports service workers.');
	}

	// Schedule File
	try {
		sched = JSON.parse(localStorage.getItem('schedule'));
		if (sched.length === undefined) {
			throw new Error('bad formatting');
		}
	} catch {
		sched = [];
		console.error('Schedule file is absent or invalid.');
	}

	sched.push(
		{
			regex: 'Hours',
			name: 'office hours'
		}
	);

	getNotif();

	try {
		// Grab wake lock
		if ('wakeLock' in navigator) {
			await navigator.wakeLock.request('screen');
			console.log('Wake lock taken');
		} else {
			console.log('Wake lock unsupported');
		}
	} catch (error) {
		console.error(error);
	}

	load();
	put(await get(last));
})();

function getNotif() {
	// Register notifications
	try {
		if ('Notification' in window) {
			Notification.requestPermission().then(status => {
				if (!('permission' in Notification)) {
					Notification.permission = status;
				}

				if (Notification.permission === 'denied' || Notification.permission === 'default') {
					notif = false;
				} else {
					notif = true;
				}
			});
		}
	} catch (error) {
		console.error(error);
	}
}

async function get(cached) {
	let content_;
	if (cached && cached.time === undefined && (toDays(cached.remote_time) === getDays())) {
		content_ = cached;
	} else {
		try {
			const headers = new Headers();
			headers.append('cache-control', 'no-cache');
			const init = {
				headers
			};
			try {
				content_ = await (await fetch('https://bell-api.spaghet.us/api/v1', init)).json();
			} catch (error) {
				console.error('something broke:', error);
			}

			last = content_;
			save();
		} catch {
			content_ = cached;
		}
	}

	if ((!content_)) {
		return false; // This will break
	}

	if (!content_.schedule) {
		return {
			period: 'no school'
		};
	}

	const time = cached ? getTime() : toMinutesFromUtc(content_.remote_time);
	const isSchoolDay = !(content_.noSchedule === 0);
	if (isSchoolDay) {
		let currentStart;
		let currentEnd;
		let currentName;
		let next = false;
		for (const i of content_.schedule) {
			const start = toMinutes(i.start);
			const end = toMinutes(i.end);
			if (start < time && time < end) {
				currentStart = start;
				currentEnd = end;
				currentName = i.name;
			} else if (start > time && !next) {
				next = i;
			}
		}

		if (!currentStart || !currentEnd || !currentName) {
			if (next) {
				return {
					nextPeriod: next.name,
					nextStart: next.start
				};
			}

			return {
				period: 'nothing',
				nextPeriod: 'tomorrow'
			};
		}

		const elapsedTime = time - currentStart;
		const periodTime = currentEnd - currentStart;
		const percent = (elapsedTime / periodTime) * 100;
		if (!cached) {
			console.log(`${elapsedTime} of ${periodTime} minutes, ${percent}%`);
		}

		return {
			period: currentName,
			percent,
			nextPeriod: next.name,
			nextStart: next.start
		};
	}

	return false;
}

async function put(got) {
	const now = getTime();
	const timeString = `${Math.floor(now / 60)}:${Math.round(now % 60).toString().padStart(2, '0')}`;
	getel('time').innerHTML = timeString;

	if (got === false) {
		getel('period').innerHTML = '!!!';
		getel('next_period').innerHTML = '!!!';
		return;
	}

	if (got.period) {
		const periodName = replacePeriodName(got.period);
		getel('period').innerHTML = periodName;
	} else {
		getel('period').innerHTML = 'no period';
	}

	if (notif) {
		if (notifCurrentPeriod !== getel('period').innerHTML) {
			if (lastNotif) {
				lastNotif.close();
			}

			lastNotif = new Notification('New Period', {
				body: `Period has changed to ${getel('period').innerHTML}`
			});

			notifCurrentPeriod = getel('period').innerHTML;
		}
	}

	if (got.percent) {
		getel('period_bar').style.width = got.percent + '%';
		getel('period_bar_container').title = `period progress ${got.percent.toFixed(0)}%`;
	} else {
		getel('period_bar').style.width = '100%';
		getel('period_bar_container').title = 'period progress unknown';
	}

	if (got.nextStart && got.nextPeriod) {
		const nextPeriodName = replacePeriodName(got.nextPeriod);
		getel('next_period').innerHTML = `${nextPeriodName} at ${got.nextStart.toLowerCase()}`;
	} else {
		getel('next_period').innerHTML = 'tomorrow';
	}
}

function replacePeriodName(name) {
	for (const i of sched) {
		if (i.regex) {
			const re = new RegExp(i.regex, 'i');
			if (re.test(name)) {
				console.log(`${name} matches ${i.regex}`);
				return i.name;
			}
		}
	}

	return name.toLowerCase();
}

function getel(id) {
	return document.querySelector(`#${id}`);
}

function toMinutes(time) {
	const pair = time.split(':');
	return (Number.parseInt(pair[0], 10) * 60) + Number.parseInt(pair[1], 10);
}

function toMinutesFromUtc(time) {
	const pair = time.split('T')[1].split(':');
	return (Number.parseInt(pair[0], 10) * 60) + Number.parseInt(pair[1], 10);
}

function toDays(date) {
	const triplet = date.split('-');
	const m = Number.parseInt(triplet[1], 10);
	const d = Number.parseInt(triplet[2], 10);
	return (m * 32) + d;
}

function getDays() {
	const now = new Date(Date.now());
	const m = now.getMonth() + 1;
	const d = now.getDate();
	return (m * 32) + d;
}

document.addEventListener('dblclick', () => {
	if (document.body.requestFullscreen) {
		document.body.requestFullscreen();
	}

	if (document.body.webkitRequestFullscreen) {
		document.body.webkitRequestFullscreen();
	}
});

document.addEventListener('click', () => {
	getNotif();
});

function getTime() { // Returns minutes since midnight
	const now = new Date(Date.now());
	const h = now.getHours();
	const m = now.getMinutes();
	const s = now.getSeconds();
	return (h * 60) + m + (s / 60);
}

function save() {
	if (typeof last !== 'string') {
		localStorage.setItem('cache', JSON.stringify(last));
	}
}

function load() {
	const cache = localStorage.getItem('cache');
	if (cache) {
		try {
			last = JSON.parse(cache);
		} catch {
			last = false;
		}
	}
}

