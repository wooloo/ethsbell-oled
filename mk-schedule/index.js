const preview = geti('json_preview');

function geti(id) {
	return document.querySelector(`#${id}`);
}

function updatePreview() {
	preview.innerHTML = escapeHtml(JSON.stringify(stored, null, 2));
}

function escapeHtml(unsafe) {
	return unsafe
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#039;');
}

function downloadString(text, fileType, fileName) {
	const blob = new Blob([text], {type: fileType});

	const a = document.createElement('a');
	a.download = fileName;
	a.href = URL.createObjectURL(blob);
	a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
	a.style.display = 'none';
	document.body.append(a);
	a.click();
	a.remove();
	setTimeout(() => {
		URL.revokeObjectURL(a.href);
	}, 1500);
}

// Storage

let stored;
try {
	stored = JSON.parse(localStorage.getItem('schedule'));
	if (!stored) {
		throw new Error('empty');
	}

	updatePreview();
} catch {
	stored = [];
	updatePreview();
}

// Elements

const periodSelect = geti('period_select');

const periodRegex = geti('period_regex');

const periodName = geti('period_name');

const update = geti('update');

const write = geti('write');

const delete_ = geti('delete');

const download = geti('download');

// Form behaviour

periodSelect.addEventListener('change', () => {
	if (periodSelect.value !== '') {
		periodRegex.value = periodSelect.value;
	}

	let found = false;
	for (const i of stored) {
		if (i.regex === periodRegex.value) {
			periodName.value = i.name;
			found = true;
			break;
		}
	}

	if (!found) {
		periodName.value = '';
	}
});

periodRegex.addEventListener('change', () => {
	periodSelect.value = '';
	for (const i of stored) {
		if (i.regex === periodRegex.value) {
			periodName.value = i.name;
		}
	}
});

// Update behaviour

update.addEventListener('click', () => {
	for (const i of stored) {
		if (i.regex === periodRegex.value) {
			i.name = periodName.value;
			updatePreview();
			return;
		}
	}

	stored.push({
		name: periodName.value,
		regex: periodRegex.value
	});
	updatePreview();
});

// Delete behaviour

delete_.addEventListener('click', () => {
	for (const i in stored) {
		if (stored[i].regex === periodRegex.value) {
			stored.splice(i, 1);
		}
	}

	updatePreview();
});

// Write behaviour

write.addEventListener('click', () => {
	localStorage.setItem('schedule', JSON.stringify(stored, null, 2));
});

// Download

download.addEventListener('click', () => {
	downloadString(JSON.stringify(stored, null, 2));
});

