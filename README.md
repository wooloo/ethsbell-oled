a fancy frontend for ethsbell's api

Hosted at https://bell.spaghet.us

Why should you use it?

* This frontend is only ~6 *kilobytes* long when gzipped - that's around 0.1% of the size of ethsbell.xyz.
* It's OLED-friendly.
  * The text will move around (using efficient code that runs on your GPU) to prevent burn-in.
  * The background is pure black (#000000), so your display knows to completely turn off those pixels to save power.
* It's mobile-friendly.
  * ETHSBell is only polled once per hour, and if the poll fails, we fall back to information we already have.
  * You only need to connect to the internet once at the start of the day.
  * You can even load the software without an internet connection, after you've loaded it for the first time.
  * Your screen won't go to sleep, so you can leave it running on your desk.
* It can understand your schedule
  * If you like, you can supply a file that describes your schedule and we'll replace period names with your classes.
  * See [the documentation](SCHEDULE.md) for details.

# Special Troubleshooting Section

* The offline caching mechanism is really jank and if something breaks, the first thing you should do is clear your cache. On mobile, I'd recommend using the app-level cache clear rather than the browser's own, since the browser might not clear everything out. On desktop, you can go to about:cache to see which folders you should remove if you're running Firefox. I honestly have no idea how the other browsers handle cache, so you're on your own.
