const CACHING_DURATION = 24 * 3600;

const ignoreRegexes = [
	/mk-schedule/,
	/cdn/,
	/font/,
	/api/
];

console.log('Loaded service worker');

self.addEventListener('install', event => {
	event.waitUntil(new Promise(resolve => {
		resolve();
	}));
	console.log('service worker is installed');
});

self.addEventListener('fetch', event => {
	// https://phyks.me/2019/01/manage-expiration-of-cached-assets-with-service-worker-caching.html

	const {request} = event;

	// ...

	event.respondWith(caches.open('v1').then(
		cache => cache.match(request).then(
			response => {
				// If there is a match from the cache
				if (response && /api/.test(request)) {
					console.log(`SW: serving ${request.url} from cache.`);
					const expirationDate = Date.parse(response.headers.get('sw-cache-expires'));
					const now = new Date();
					// Check it is not already expired and return from the
					// cache
					if (expirationDate > now) {
						return response;
					}
				}

				// Otherwise, let's fetch it from the network
				console.log(`SW: no match in cache for ${request.url}, using network.`);
				// Note: We HAVE to use fetch(request.url) here to ensure we
				// have a CORS-compliant request. Otherwise, we could get back
				// an opaque response which we cannot inspect
				// (https://developer.mozilla.org/en-US/docs/Web/API/Response/type).
				return fetch(request.url).then(liveResponse => {
					if (!liveResponse.ok && response) {
						return response;
					}

					// Compute expires date from caching duration
					const expires = new Date();
					expires.setSeconds(
						expires.getSeconds() + CACHING_DURATION
					);
					// Recreate a Response object from scratch to put
					// it in the cache, with the extra header for
					// managing cache expiration.
					const cachedResponseFields = {
						status: liveResponse.status,
						statusText: liveResponse.statusText,
						headers: {'SW-Cache-Expires': expires.toUTCString()}
					};
					liveResponse.headers.forEach((v, k) => {
						cachedResponseFields.headers[k] = v;
					});
					// We will consume body of the live response, so
					// clone it before to be able to return it
					// afterwards.
					const returnedResponse = liveResponse.clone();
					return liveResponse.blob().then(body => {
						console.log(
							`SW: caching tiles ${request.url} until ${expires.toUTCString()}.`
						);
						// Put the duplicated Response in the cache
						let good = true;
						for (const i of ignoreRegexes) {
							if (
								i.test(request.url)
							) {
								good = false;
								break;
							}
						}

						if (good) {
							cache.put(request, new Response(body, cachedResponseFields));
						}

						// Return the live response from the network
						return returnedResponse;
					});
				}).catch(() => {
					// Last try, use old cache if nothing is working
					if (response) {
						return response;
					}
				});
			})
	)
	);
});
